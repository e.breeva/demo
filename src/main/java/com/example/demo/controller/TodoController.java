package com.example.demo.controller;

import com.example.demo.model.Todoitem;
import com.example.demo.repo.Todorepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/todo")
public class TodoController {
    @Autowired
    private Todorepo todorepo;
    @GetMapping
    public List<Todoitem> findAll(){
        return todorepo.findAll();
    }

    @PostMapping
    public Todoitem save(@Valid @NotNull @RequestBody Todoitem todoitem){
        return todorepo.save(todoitem);
    }
}
