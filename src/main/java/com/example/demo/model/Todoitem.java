package com.example.demo.model;

import javax.persistence.Entity;


@Entity
public class Todoitem {

    private long id;
    @NotBlank
    private string title;
    private boolean done;

    public Todoitem() {
    }

    public Todoitem(long id, string title, boolean done) {
        this.id = id;
        this.title = title;
        this.done = done;
    }
    @id
    @GeneratedValue
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public string getTitle() {
        return title;
    }

    public void setTitle(string title) {
        this.title = title;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
